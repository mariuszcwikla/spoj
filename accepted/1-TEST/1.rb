#!ruby
until $stdin.eof
	a = gets.strip
	#TIP: DO NOT USE "return" but "break".
	#otherwise "runtime error" in spoj :(
	break if a=="42"
	puts a
end
