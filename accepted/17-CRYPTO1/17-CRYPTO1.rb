#!ruby

def modexp(a,b,n)
	return 1 if b==0
	return a if b==1
	exponents = { 0 => 1, 1=>a }    
	i = 2    
	squared = a
	while i<=b
	  squared = squared * squared % n
	  exponents[i] = squared
	  i*=2
	end

	exponent = i/2
	v = exponents[exponent]

	add_to_exponent = exponent/2 
	while exponent <b
	  next_exponent = exponent + add_to_exponent 
	  if next_exponent == b
		return (v * exponents[add_to_exponent]) % n
	  elsif next_exponent > b
		  add_to_exponent/=2
	  else
		  v= (v * exponents[add_to_exponent]) % n      
		  exponent = next_exponent
	  end
	end

	raise "current!=b: #{exponent} <-> #{b}" unless exponent==b
	return v
end

=begin

M = 4000000007

x^2 = E mod M

x^(M-1)=1

Ponieważ M jest parzyste, M-1 jest nieparzyste

x^2 * x^2 * ... x^2 = x^(M-1) = 1
2 + 2 + .. + 2 = M-1			
2 * (M-1)/2 = M-1			//multiply and divide by 2

E * E * E ... * E = x^(M-1) = 1
E^((M-1)/2)=x^(M-1)=1



x^(M-1) * x = x
E^((M-1)/2)*x=x

4000000007+1 divides by 4
x^(M+1)=x^2=E
E^((M+1)/2)=x^(M+1)=x^2		//square it
E^((M+1)/4)=x

	  
=end

enc_time = gets.to_i
modulus = 4000000007

#max = Time.new(2029, 12, 31, 23, 59, 59, 59).to_i 
#max = Time.new(2030, 1, 1, 0,0,0).to_i 
max = 1893456000 		#to zdarłem z forum, ale w praktyce chodzi o liczbe milisekund do roku 2030

exp = modexp(enc_time, (modulus+1)/4, modulus)
#should be (1087143639**2) % 4000000007

#mine (2912856368**2)%4000000007
#both give 1749870067
#on forum they say that there is one more valid value

#ponieważ są dwa pierwiastki, np.
#sqrt(25)=5 oraz -5 - na tej samej zasadzie ponizej:
exp = -exp + modulus if exp > max
puts Time.at(exp).strftime("%a %b %d %H:%M:%S %Y")
