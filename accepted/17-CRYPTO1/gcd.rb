
a = 2
b = 15

def gcd(a,b)
	a = a%b
	while a>0
		a,b = b%a, a
	end

	return b
end


=begin
działa
(2..15).each {|a|
	(2..15).each{|b| puts "gcd(#{a},#{b})=#{gcd(a,b)}"}
}
=end

def modular_inverse(a,n)
	b = n
	a = a%n
	t0 = 0
	t1 = 1
	while a>1
		r = b/a
		t0, t1 = t1, t0- t1 * r
		a,b = b%a, a
	end

	return t1%n
end

require 'scanf'

until $stdin.eof
	a,b = gets.scanf("%d%d")
	puts modular_inverse(a,b)
end