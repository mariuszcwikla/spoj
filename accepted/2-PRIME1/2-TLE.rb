#!ruby
require 'scanf'


N=1000000

$sieve = Array.new(N)

$sieve[0]=true
$sieve[1]=true
$sieve[2]=false

(2..N).each do |i|
  if !$sieve[i]
    k = 2*i
    while k<N do
      $sieve[k]=true
      k += i
    end
  end
end

def is_prime(n)
	if n<N
		return !$sieve[n]
	else
		s = Math.sqrt(n).to_i
		return false if n%2==0
			
		i = 3
		while i<s		
			if n % i == 0
				return false
			end
			i+=2
		end
		return true
	end
end

t = gets.to_i
t.times do |i|
  a,b = gets.scanf("%d%d")  
  (a..b).each do |x|
    puts x if is_prime(x)	  
  end  
  puts "" if i!=t-1 
end

