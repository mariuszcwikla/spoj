#!ruby
require 'scanf'

#po wprowadzeniu szukania bineranego nie wize roznicy....

N=32000

$primes = [2]

i = 3
while i<N
	isprime=true
	$primes.each do |p|
		if i%p == 0
			isprime = false
			break
		end
	end
	($primes  << i) if isprime
	i+=2
end

def is_prime_binary_search(n, lo, hi)
	if lo==hi
		return $primes[lo]==n
	else
		mid = (lo+hi)/2
		p = $primes[mid]
		return true if p==n
		return is_prime_binary_search(n, lo, mid) if n < p
		return is_prime_binary_search(n, mid+1, hi)
	end
end

def is_prime(n)
	return false if n<=1
	if n<=N
		return is_prime_binary_search(n, 0, $primes.length)
	end
	s = Math.sqrt(n).to_i
	$primes.each do |p|
		return true if p==n
		return false if n%p==0
		return true if p>s
	end
	return true
end

t = gets.to_i
t.times do |i|
  a,b = gets.scanf("%d%d")  
  (a..b).each do |x|
    puts x if is_prime(x)	  
  end  
  puts "" if i!=t-1 
end

