#!ruby
require 'scanf'
require 'set'

N=32000

$primes = Set.new [2]

i = 3
while i<N
	isprime=true
	$primes.each do |p|
		if i%p == 0
			isprime = false
			break
		end
	end
	($primes  << i) if isprime
	i+=2
end

def is_prime(n)
	return false if n<=1	
	return true if $primes.include? n	
	s = Math.sqrt(n).to_i
	$primes.each do |p|
		return true if p==n
		return false if n%p==0
		return true if p>s
	end
	return true
end

t = gets.to_i
t.times do |i|
  a,b = gets.scanf("%d%d")  
  (a..b).each do |x|
    puts x if is_prime(x)	  
  end  
  puts "" if i!=t-1 
end

