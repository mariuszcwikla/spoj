#!ruby
require 'scanf'


N=32000

def build_sieve(n)	
	sieve = Array.new(n){false}
	sieve[0]=true
	sieve[1]=true
	sieve[2]=false
	for i in 2..(n-1)
	  if !sieve[i]
		k = i*i
		while k<n do
		  sieve[k]=true
		  k += i
		end
	  end
	end
	return sieve
end

def build_segment(a, b, sieve1)
	sieve2=Array.new(b-a+1){false}
	for i in 2..(sieve1.size-1)
		if sieve1[i]==false
			j = a%i
			if j>0
				j = i-j
			end			
			while j<sieve2.size
				sieve2[j]=true
				j+=i
			end
		end
	end
	return sieve2
end




def is_prime(n, a, sieve1, sieve2)
	return false if n<=1	
	if n<sieve1.size
		return sieve1[n]==false
	else
		return sieve2[n-a]==false
	end
end

t = gets.to_i


sieve1 = build_sieve(N)

t.times do |i|
  a,b = gets.scanf("%d%d")  
  #k = Math.sqrt(b).to_i+1
  #sieve1=build_sieve(k)
  #puts "building sieve #{k}"
  
  sieve2 = nil
  if b>=sieve1.size
	sieve2=build_segment(a,b,sieve1)
	#puts "building segmented sieve #{a}-#{b}"
  end
  (a..b).each do |x|
    puts x if is_prime(x, a, sieve1, sieve2)	  
  end  
  puts "" if i!=t-1 
end

