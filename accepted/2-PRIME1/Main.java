import java.util.Scanner;
public class Main{
	static int N = 1_000_000;
	static boolean [] sieve = new boolean[N];
	
	public static boolean isPrime(int n){
		if(n<N)
			return !sieve[n];
		else{
			if(n%2==0)
				return false;
			int s = (int)Math.sqrt(n);
			for(int i=3;i<s;i+=2){
				if(n%i==0)
					return false;
			}
			return true;
		}
	}
	
	public static void main(String [] args){
		
		sieve[0]=sieve[1]=true;
		for(int i=2;i<N;i++){
			if(sieve[i]==false){
				int k = i*2;
				while(k<N){
					sieve[k]=true;
					k += i;
				}
			}
		}
		
		Scanner s = new Scanner(System.in);
		int t = s.nextInt();
		
		
		
		while(t-->0){
			int a = s.nextInt();
			int b = s.nextInt();
			while(a<=b){
				if (isPrime(a))
					System.out.println(a);
				++a;
			}
		}
		
	}
}