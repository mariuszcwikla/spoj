#include <stdio.h>
#include <set>
#include <math.h>
using namespace std;
const int N = 32000;

bool is_prime(set<int>& primes, int x){
    if(x<=1)
        return false;
	if(x<=N){
		return primes.find(x)!=primes.end();		
	}
    //int s = sqrt(x) 

    for(set<int>::iterator it=primes.begin();it!=primes.end();it++){
        int v = *it;
        if(v==x)
            return true;
        if(x%v==0)
            return false;
        if(v*v>x)
            return true;
    }
    return true;
}

int main(){
    set<int> primes;
    primes.insert(2);
    bool isprime;
    for(int i=3;i<N;i++)
    {
        isprime=true;
        for(set<int>::iterator it = primes.begin(); it!=primes.end();it++){
            if (i%(*it)==0){
                isprime=false;
                break;
            }
        }
        if(isprime)
            primes.insert(i);

        
    }

    int t,a,b;
    scanf("%d",&t);
    while(t--){
        scanf("%d%d",&a,&b);
        while(a<=b){
            if(is_prime(primes,a))
                printf("%d\n", a);
            a++;
        }
        if(t>0)
            puts("");

    }

    return 0;
}
