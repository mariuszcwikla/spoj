#include <stdio.h>
#include <math.h>
#include <string.h>

const int N = 32000;

#define PRIME 1
#define COMPOSITE 0

char sieve[N];

void build_sieve(){
    sieve[0]=COMPOSITE;        //not a composite, but not prime either ;)
    sieve[1]=COMPOSITE;         //not a composite, but not prime either ;)
    int n=sqrt(N)+1;
    for(int i=2;i<n;i++){
        if(sieve[i]==COMPOSITE)
            continue;
        int j = i*2;
        while(j<N){
            sieve[j]=COMPOSITE;
            j+=i;
        }
    }
}

bool is_prime(int x, int a, char* segment){
    if(x<=1)
        return false;
    if(x<N){
        return sieve[x]==PRIME;
    }else{
        return segment[x-a]==PRIME;
    }   
}

void build_segment(int a, int b, char* segment){
    memset(segment, PRIME, b-a+1);
    int range = b-a+1;
    for(int i=2; i < N;i++){
        if(sieve[i]==PRIME){
            int j = a%i;
            if(j>0)
                j=i-j;
            if(j>=range)
                break;
            while(j<range){
                segment[j]=COMPOSITE;
                j+=i;
            }
        }
    }
}


int main(){
    build_sieve();
    memset(sieve, PRIME, sizeof(sieve));
    char* segment = new char[100000+1];
        
    int t,a,b;
    scanf("%d",&t);
    while(t--){
        scanf("%d%d",&a,&b);
        
        if(b>=N)
            build_segment(a,b, segment);
        int x = a;
        while(x<=b){
            if(is_prime(x, a, segment))
                printf("%d\n", x);
            x++;
        }
        if(t>0)
            puts("");

    }
    delete [] segment;
    return 0;
}
