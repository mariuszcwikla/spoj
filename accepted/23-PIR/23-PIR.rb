#!ruby
require 'scanf'
require 'matrix'

def volume_of_tetrahedron(ab,ac,ad,bc,bd,cd)
	#based on formula http://mathforum.org/dr.math/faq/formulas/faq.irreg.tetrahedron.html
	#tetrahedron bottom is ABC
	#top is D
	m = Matrix[ 
		[0, ab**2, ac**2, ad**2, 1],
		[ab**2, 0, bc**2, bd**2, 1],
		[ac**2, bc**2, 0, cd**2, 1],
		[ad**2, bd**2, cd**2, 0, 1],
		[1, 1, 1, 1, 0]
	]
	vv = m.det
	return Math.sqrt(vv/288.0)
end


t = gets.to_i
t.times do |i|
=begin
  so stupid: description says "integers", this one
  a,b,c,d,e,f = gets.scanf("%d%d%d%d%d%d")
  not works - results in WA.
  
  After changing to floats it works.
  
  Looks like they give floats on input too.
=end
  a,b,c,d,e,f = gets.scanf("%f%f%f%f%f%f")
  v = volume_of_tetrahedron(a,b,c,d,e,f)
  puts "#{v.round(4)}"
end

