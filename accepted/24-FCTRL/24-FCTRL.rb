#!ruby

factorials = Array.new(101)
factorials[1]=1
(2..100).each  {|i| factorials[i] = factorials[i-1] * i}

t = gets.to_i
t.times do |i|
  a = gets.to_i
  puts factorials[a]
end

