#!ruby

$output_string = " " * (26+5)	#use global variable for optimization

class Node
	attr_accessor :children, :leaf, :count
	def initialize(leaf=false)				
		if leaf
			@count = 1
		else
			@children = Array.new(10)
		end
	end
	
	def inc
		@count+=1
	end
	
	def add(account)
		node = self
		#poniewaz są dodatkowe spacje
		#i szkoda mi wywolywac split, join, strip...
		#wiec zliczam liczbe cyfr aby dowiedziec sie czy jest koniec 
		numdigits = 0  
		account.each_char.with_index {|c, i|			
			c = c.ord - '0'.ord			
			next if c<0 or c>9
			
			isleaf = numdigits == 25
			numdigits+=1					
		    
			if node.children[c]==nil
				node.children[c] = Node.new(isleaf)
				node = node.children[c]
			else
				node = node.children[c]
				node.inc if isleaf				
			end
			
			break if isleaf
		}
	end
	
	def dfs_and_print_recursive(level, position)
		
	  if level<2
      shift = 0 
	  elsif level<10
	    shift = 1 
    elsif level<14
      shift = 2 
    elsif level<18
      shift = 3 
	  elsif level<22
	    shift = 4
	  else
	    shift = 5
	  end 
	  
		$output_string[level + shift] = ('0'.ord + position).chr
						
		if @count!=nil
			puts "#{$output_string} #{@count}"
		else		
			@children.each_with_index {|c, i|
				if c!=nil
					c.dfs_and_print_recursive(level+1, i)
				end
			}
		end
	end
	
	def dfs_and_print
		@children.each_with_index {|c, i|			
			if c!=nil								
				c.dfs_and_print_recursive(0, i)			
			end
		}		
	end
end

class AccountTree
	def initialize
		@root = Node.new
	end
	
	def add(account)		
		@root.add(account)
	end
	
	def print
		@root.dfs_and_print
	end
end


t = gets.to_i
t.times do |i|
  a = gets.to_i
  tree = AccountTree.new
  
  a.times do 
  	account = gets
  	tree.add(account)
  end
  gets
  tree.print
  puts "" if i!=t-1
  
end
