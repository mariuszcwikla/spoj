#!ruby

# OK... probowalem zastosowac prefix tree, ale dzialalo zbyt wolno - 27-SBANK-solution1.rb
# drugie podejscie - zwykły hash + sort_by i przeszło w czasie 0.93 ale nie ma mnie na liscie najlepszych wyników :(

#Ciekawe - na liscie globalnej jestem widoczny
# http://www.spoj.com/ranks/SBANK/start=3360
# czas 0.93
# ale na liscie RUBY juz nie :(
# http://www.spoj.com/ranks/SBANK/lang=RUBY,start=0  a powinienem byc drugi!!

t = gets.to_i
t.times do |i|
  a = gets.to_i
  
  accounts = {}
  
  a.times do 	
  	account = gets.strip
	if accounts[account]==nil
		accounts[account]=1
	else
		accounts[account] = accounts[account] + 1
	end  	
  end
  gets
  
  accounts.sort_by {|k, v| k}.each{|k,v| puts "#{k} #{v}"}
  puts "" if i!=t-1
  
end
