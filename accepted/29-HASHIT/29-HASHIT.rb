#!ruby


def hash(s)
	h = 0
	s.each_char.with_index do |c, i|
		h+=c.ord * (i+1)
	end
	return (h*19) % 101
end


def insert(table, s)
	h = hash(s)
    #puts "hash(#{s})=#{h}"
	inserted = false
	firstidx = nil
	for i in 0..19
		hh = (h + i*i + 23 * i) % 101
		if table[hh]==s
			return
		elsif table[hh]==nil and firstidx==nil
			firstidx = hh
		end
	end
	#puts s + ": " + i.to_s + " hash=" + h.to_s	
	table[firstidx]=s if firstidx!=nil
end

def delete(table, s)
	h = hash(s)
	for i in 0..19
		hh = (h + i*i + 23 * i) % 101
		if table[hh]==s
			table[hh]=nil
			return
		end
	end		
end

t = gets.to_i
t.times do |i|
  a = gets.to_i
  
  table = Array.new(101, nil)
  
  a.times do |idx|
	line = gets.chomp
	word = line[4..-1].strip
	if line.start_with? "ADD:"	  	  
	  insert(table, word)
	else	  
	  delete(table, word)
	end
  end
  
  puts table.count {|x| x!=nil}
  table.each_with_index do |s, i|
	puts "#{i}:#{s}" if s!=nil
  end
  
  #There should be a blank line after every test case .. that cost me 2WA
  puts ""
end

