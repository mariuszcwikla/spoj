#!ruby

require("scanf")


def dfs(map, c, r, x, y)
	maxx, maxy, max = x, y, 0
	locations = [[x,y]]
	stack = []
	stack.push([x,y])
	map[y][x]=0
	while not stack.empty?
		x,y=stack.pop
		#puts "#{x},#{y}=#{map[y][x]}"
		v = map[y][x]
		if x<c-1 and map[y][x+1]=='.'
			stack.push([x+1,y])
			map[y][x+1]=v+1
			locations<<[x+1,y]
		end
		if x>0 and map[y][x-1]=='.'
			stack.push([x-1, y])
			map[y][x-1]=v+1
			locations<<[x-1,y]
		end
		if y<r-1 and map[y+1][x]=='.'
			stack.push([x, y+1])
			map[y+1][x]=v+1
			locations<<[x,y+1]
		end
		if y>0 and map[y-1][x]=='.'
			stack.push([x, y-1])
			map[y-1][x]=v+1
			locations<<[x,y-1]
		end		
		if v > max
			maxx, maxy, max = x, y, v
		end
	end
	return [locations, maxx, maxy, max]
end

def calc_len(map, c, r, x, y)
		
	locations, x, y, max = dfs(map, c, r, x, y)
	#reset	
	#puts "max=#{max}"
	
	locations.each {|x,y| map[y][x]='.' }	
	
	locations, x, y, max = dfs(map, c, r, x, y)
		
	return max
end


def calc_max_len(map, c, r)
	max = 0
	for y in (0..r-1)
		line = map[y]
		for x in (0..c-1)
			if line[x]=='.'
				len = calc_len(map, c, r, x, y)
				if len > max
					max = len
				end
			end
		end
	end
	return max
end

t = gets.to_i
t.times do |i|
  c,r = gets.scanf("%d%d")
  map = Array.new(r)
  for i in (0..r-1)
	map[i]=gets.strip.chars
  end
  max_len = calc_max_len(map, c, r)
  puts "Maximum rope length is #{max_len}."
end

