#!ruby

def to_onp(line)
	prio = {'+' => 1, '-' => 1, '*' => 2, '/' => 2, '^' => 3}
	
	opstack = []
	out = ""
	line.strip.each_char do |c|
		if c=='('
			opstack.push(c)
		elsif c==')'
			until opstack.empty? or (cc=opstack.pop)=='('
				out<<cc
			end
		else
			if prio.include? c				
				until opstack.empty? 
					if opstack.last == '('
						#opstack.pop
						break
					elsif prio[opstack.last] <= prio[c]						
						break
					else						
						out << opstack.pop
					end
				end	
				opstack.push c
			else
				out<<c
			end
		end
	end	
	
	until opstack.empty?
		out<<opstack.pop
	end
	return out
end

t = gets.to_i
t.times do |i|
  a = gets  
  puts to_onp(a)
end

