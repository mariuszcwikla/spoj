#!ruby



=begin

12345 -> 124 + 21

123 45

123 12 -


IDEA: odkrój elementy [n/2+1..n]
odwroc je i doklej do liczby
jesli otrzymana jest liczba wieksza to wyswietl

jak nie, to wez elementy [1..n/2-1], je odwroc i doklej na koniec

=end

def next_palin(str)
	len = str.length
	n = str.to_i
	n+=1
	#special case: 9, 99, 999, 999... easiest is to check if length has increased ;)
	after = n.to_s
	if after.length==str.length+1
		return (n+1).to_s
	end
	
	#corner case: 1000, 1000, 2001
	return n if n.to_s == n.to_s.reverse
	
	#WARNING: nie jest to optymalny algorytm, ale dziala :)
	
	if len%2==0
		half = str[0..(len/2-1)]
		#puts half
		a = half + half.reverse
		h2 = (half.to_i + 1).to_s
		b = h2 + h2.reverse
		a = a.to_i		
		b = b.to_i
		return b if a<=n		
		return [a,b].min
	else
		half = str[0..(len/2)]
		a = half + (half[0..-2].reverse)
		h2 = (half.to_i + 1).to_s
		b = h2 + h2[0..-2].reverse		
		a = a.to_i
		b = b.to_i
		return b if a<=n		
		return [a,b].min
	end
end

t = gets.to_i
t.times do |i|
  a = gets.strip
  puts next_palin(a)
end

