#!ruby


def add(a,b)
  c = a.to_i + b.to_i
  c = c.to_s
  na = a.size
  nb = b.size
  nc = c.size
  n = [na,nb+1,nc].max
  
  s  = a.rjust(n) + "\n"
  s += ("+" + b).rjust(n) + "\n"
  s += ("-"*n) + "\n"
  s += c.rjust(n)
end

def sub(a,b)
  c = a.to_i - b.to_i
  c = c.to_s
  na = a.size
  nb = b.size
  nc = c.size
  n = [na,nb+1,nc].max
  
  s  = a.rjust(n) + "\n"
  s += ("-" + b).rjust(n) + "\n"
  s += ("-"*([nb+1, nc].max)).rjust(n) + "\n"
  s += c.rjust(n)
end

def mul(a,b)  
  na = a.to_i
  nb = b.to_i
  c = na * nb
  c = c.to_s
  sa = a.size
  sb = b.size
  sc = c.size
  s = [sa,sb+1,2,c.size].max 
  #s = c.size
  
  str  = a.rjust(s) + "\n"
  str += ("*" + b).rjust(s) + "\n"
  if nb>9
    b.size.times{|i|
      digit = b[b.size-i-1].ord - '0'.ord
      v = (digit * na).to_s
      if i==0
        str += ("-"*([sb+1,v.size].max)).rjust(s) + "\n"
        #str += ("-"*([sa,sb+1,v.size].max)).rjust(s) + "\n"
      end
      str += v.rjust(s-i) + "\n"
    }
    str += ("-"*c.size).rjust(s) + "\n"
  else
    str += ("-"*[sb+1,c.size].max).rjust(s) + "\n"
  end

  str += c.rjust(s)
end

t = gets.to_i
t.times do |i|
  line = gets.strip
  if line.include? "+"
    a,b=line.split "+"
    puts add(a,b)
  elsif line.include? "-"
    a,b=line.split "-"
    puts sub(a,b)
  else
    a,b=line.split "*"
    puts mul(a,b)
  end
  puts ""
end

