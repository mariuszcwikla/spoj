#!ruby
require 'scanf'

def diff_method(x)
    
    sum = x.last
    diffs = [sum]
    while x.size>1
        #1.upto(x.size-1) do |i|
		#for i in 1..(x.size-1)
		k = x.size
		i = 1
		while i < k					#zmiana 1.upto na while - przyspiesezylo o 1 sekunde
            x[i-1] = x[i]-x[i-1]
			i+=1
        end
        x.pop
        sum += x.last
		diffs << x.last
    end
    return [sum, diffs]
end

#t = scanf("%d")[0]
t = gets.to_i
t.times do |i|
    #s,c=scanf("%d%d")
	s,c=gets.split
	s=s.to_i
	c=c.to_i
    #x = []
    #s.times do        
    #    k = scanf("%d")[0]
    #    x << k
    #end
	x = gets.split.map &:to_i
    d = 0
	sum, diffs = diff_method(x.dup)
	#print sum
	(c).times do |i|	
		j = diffs.size-1
		#for j in (diffs.size-1)..1
		while j>=1
			diffs[j-1]+=diffs[j]
			j-=1
		end
		print " " if i>0
		print "#{diffs[0]}"
	end
	puts ""
end

