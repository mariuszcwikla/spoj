#!ruby

class SegmentTreeArrayBased
  attr_reader :function, :iterations
  def initialize(arr, function)
    @function = function
    @num_leaves = arr.length
    #@nodes = Array.new(2 * arr.length - 1)
    @nodes = Array.new(4 * arr.length)    # Need 4*N space because algorithm creates "holes" in the array
    build(arr, 0, 0, arr.length)
  end

  def query(a, b)
    @iterations = 0

    left = 0
    right = @num_leaves
    query_internal(a, b, 0, left, right)
  end

  private def query_internal(a, b, n, left, right)
    @iterations += 1
    if a == left && b == right
      @nodes[n]
    else
      mid_node = (left + right) / 2
      if b <= mid_node
        query_internal(a, b, n * 2 + 1, left, mid_node)
      elsif a >= mid_node
        query_internal(a, b, n * 2 + 2, mid_node, right)
      else
        m = (a + b )/2 
        vl = query_internal(a, mid_node, n * 2 + 1, left, mid_node)
        vr = query_internal(mid_node, b, n * 2 + 2, mid_node, right)
        @function.call(vl, vr)
      end
    end
  end

  private def build(array, n, a, b)
    # Note: this implementation makes "holes" in the node array
    # because it does not create perfectly balanced tree
    # Implementing tree without holes is tricky. I couldn't develop good algorithm to do it :(
    if n >= @nodes.size
      raise "out of bounds #{n}, array.size=#{array.size}, total num of nodes=#{@nodes.size}"
    end
    if a == b - 1
      @nodes[n] = array[a]
    else
      mid = (a + b) / 2
      # mid = a + (b - a)/2
      left = build(array, n * 2 + 1, a, mid)
      right = build(array, n * 2 + 2, mid, b)
      @nodes[n] = @function.call(left, right)
    end
  end
end

# This builds euler tour, third case in https://codeforces.com/blog/entry/18369?
def euler_tour(tree, parents, root)
	tour = []
	stack = [[root, :in, 0]]
	height = {root => 0}
	until stack.empty?
		n, type, h = stack.pop
		
		if type == :in
			tour << n
			stack << [n, :out]
			tree[n].reverse.each do |c|
				stack << [c, :in, h + 1]
				height[c] = h+1
			end
		else
		  tour << parents[n] unless parents[n].nil?
		end
	end
	[tour, height]
end

# Trying to implement LCA based on euler tour - https://codeforces.com/blog/entry/18369?
def lca_segtree(tree, parents, v, w, segtree, first, heights)
	return v if v==w

	a = first[v]
	b = first[w]
	a, b = b, a if b < a
	
	h = segtree.query(a, b)
	h.v
end

def lca(tree, parents, v, w)
	return v if v==w
	visited = {}
	until v.nil?
		v = parents[v]
		visited[v]=true
	end
	until w.nil?
		w = parents[w]
		return w if visited[w]
	end
	return nil
end

class HeightAndIndex
  attr_reader :h, :v
  def initialize(h, v)
	@h = h
	@v = v
  end
end

t = ARGF.gets.to_i
t.times do |i|
  nodes = ARGF.gets.to_i
  tree = {}
  parents = {}
  1.upto(nodes) do |node|
	tree[node] = []
	ARGF.gets.chomp.split[1..-1].each do |child|
		child = child.to_i
		tree[node] << child
		parents[child] = node
	end
  end
  q = ARGF.gets.to_i
  
  root = tree.first[0]
  root = parents[root] until parents[root].nil?
  euler, height = euler_tour(tree, parents, root)
  
  first = {}
  hh = euler.map{|v| HeightAndIndex.new(height[v], v)}
  
  euler.each_with_index do |e, i|
	if first[e].nil?
		first[e] = i
	end
  end
  
  segtree = SegmentTreeArrayBased.new(hh, ->(a, b){
    if a.h < b.h
	  a
	else
	  b
	end
  })
  puts "Case #{i+1}:"
  q.times do
	v, w = ARGF.gets.chomp.split.map(&:to_i)
	puts lca_segtree(tree, parents, v, w, segtree, first, height)
  end
end

