#!ruby
# Mam zaakceptowane rozwiazanie w oparciu o Euler Tour i Segment Tree, ale próbowałem
# tez zaimplementowac w oparciu o SQRT Decomposition
# Niestety dosaję WA, chociaż algorytm wydaje się prawidłowo działać
# Ponadto czas w Segment Tree mam 0.71, zaś w SQRT decompition - 1.08
# Jednakże algorytm oparty o SQRT decomposition jest DUZO PROSTSZY!
def sqrt_decomposition(tree, parents, root)
	height = {}
	height[root]=0
	parents[root]=root
	compressed = Array.new(parents.size+1)		# +1 because nodes are 1-based
	
	segment_size = Math.sqrt(parents.size).to_i
	
	compressed[root]=root
	
	stack = [root]
	until stack.empty?
		n = stack.pop
		h = height[n]
		seg, mod = h.divmod segment_size
		tree[n].each do |v|
			height[v]=h+1
			stack << v
		end
		
		
		compressed[n] = if mod==0		# top of segment
							parents[n]
						else
							compressed[parents[n]]
						end
	end
	
	
	[compressed, height]
end

def lca_sqrt(tree, parents, v, w, compressed, height)
	while compressed[v]!=compressed[w]
		if height[v] < height[w]
			w = compressed[w]
		else
			v = compressed[v]
		end
	end
	
	while v != w
		if height[v] < height[w]
			w = compressed[w]
		else
			v = compressed[v]
		end
	end
	v
end


t = ARGF.gets.to_i
t.times do |i|
  nodes = ARGF.gets.to_i
  tree = {}
  parents = {}
  1.upto(nodes) do |node|
	tree[node] = []
	ARGF.gets.chomp.split[1..-1].each do |child|
		child = child.to_i
		tree[node] << child
		parents[child] = node
	end
  end
  q = ARGF.gets.to_i
  
  root = tree.first[0]
  root = parents[root] until parents[root].nil?
  parents[root] = nil	# add this explicitely to make parents.size equal to num of nodes
  
  compressed, height = sqrt_decomposition(tree, parents, root)
  puts "Case #{i+1}:"
  q.times do
	v, w = ARGF.gets.chomp.split.map(&:to_i)
	puts lca_sqrt(tree, parents, v, w, compressed, height)
  end
end

