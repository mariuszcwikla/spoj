def rhash(x, a = 0, b = x.length - 1)
  h = 0
  a.upto(b) do |i|
	h = h ^ x[i].hash
  end
  h
end

def match(a, b, i, ha, hb)
  return false unless ha==hb
  
  return true if a==b[i..(i+a.length-1)]
end

def find_occurrences(a, b)
  locations = []
  ha = rhash(a)
  hb = rhash(b, 0, a.length - 1)
  locations << 0 if match(a, b, 0, ha, hb)
  1.upto(b.length - a.length) do |i|
    hb = hb ^ b[i-1].hash ^ b[i + a.length - 1 ].hash
	locations << i if match(a, b, i, ha, hb)
  end
  locations
end

first = true
while line=STDIN.gets
  n = line.to_i
  a = STDIN.gets.chomp
  b = STDIN.gets.chomp
  locations = find_occurrences(a, b)
  puts "" unless first
  first = false
  if locations.size==0
    
  else
    puts locations.join("\n")
  end
end

