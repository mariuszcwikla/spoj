#!ruby
require 'set'

def collect(previous, encrypted)
	for i in 0..([previous.size, encrypted.size].min)
		a=previous[i]
		b=encrypted[i]
		if a!=b
			return a,b
		end
	end
	return nil
end

def findtail(h)
	vals = Set.new(h.values)
	keys = Set.new(h.keys)
	
	tails = keys-vals
	if tails.size==1
		return tails.first
	else
		return nil
	end
end

CANNOT_DECRYPT="Message cannot be decrypted."

def decrypt(h, a, message)
	t = findtail(h)
	return CANNOT_DECRYPT if t==nil
		
	visited={}
	list = []
	x=t
	while x!=nil
		return CANNOT_DECRYPT if visited[x]			#cycle detected
		list << x
		visited[x]=true
		x=h[x]
	end
	
	#puts list.join " 	"
	return CANNOT_DECRYPT if list.size!=a
	
	table={}
	a.times do |i|
		table[list[list.size-1-i]]= (i+'a'.ord).chr
	end
	
	s = ""
	
	message.each_char{|c|
		if c>='a' and c<='z'
			#i = c.ord-'a'.ord
			s << table[c]
		else
			s << c
		end	
	}
	return s
end

h = {}

chars=Set.new

t = gets.to_i
t.times do |i|
  size,k = gets.split.map &:to_i
  
  previous=nil
  
  cannotdecrypt=false
  
  k.times{
	encrypted=gets.strip
	if previous!=nil
		a,b=collect(previous, encrypted)
		if a
			chars << a
			chars << b
			if h[a]==b
				cannotdecrypt=false
			end
			h[b]=a
		end
	end
	previous=encrypted
  }
  message=gets.strip
  puts decrypt(h,size,message)

end

