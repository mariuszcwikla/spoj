#!ruby

# Dostaję TLE :(

def pour1(a,b,c)
    q = [[0, 0, 0]]
    visited = {}
    visited[[0,0]]=1
    while !q.empty?
        x, y, n = q.shift
        t1 = [a-x, y].min
        t2 = [b-y, x].min
        steps = [
            [0, y],
            [x, 0],
            [a, y],
            [x, b],
            [x + t1, y-t1],
            [x - t2, y+t2]
        ].each do |xp, yp|
            next if visited[[xp, yp]]==1
            visited[[xp, yp]]=1
            q << [xp, yp, n+1]
            return n+1 if xp == c || yp == c
        end
    end
    return -1
end

t = STDIN.gets.to_i
t.times do |i|
  a = STDIN.gets.to_i
  b = STDIN.gets.to_i
  c = STDIN.gets.to_i
  puts pour1(a,b,c)
end

