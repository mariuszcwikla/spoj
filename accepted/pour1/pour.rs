use std::io;
use std::collections::{HashSet, VecDeque};

fn pour1(a: i32, b: i32, c: i32) -> i32{
    let mut q = VecDeque::new();
    q.push_back((0,0,0));
    let mut visited = HashSet::new();
    visited.insert((0, 0));
    while !q.is_empty(){
        let (x, y, n) = q.pop_front().unwrap();
        macro_rules! pour {
            ($a:expr, $b:expr) => {
                
                if !visited.contains(&($a, $b)){
                    visited.insert(($a, $b));
                    q.push_back(($a, $b, n+1));
                    if $a==c || $b==c{
                        return n+1;
                    }
                }
            };
        }
        pour!(0, y);
        pour!(x, 0);
        pour!(a, y);
        pour!(x, b);
        
        let t = (a - x).min(y);
        pour!(x + t, y - t);
        let t = (b - y).min(x);
        pour!(x - t, y + t);
    }
    -1
}

fn read_int() -> i32 {
    let mut input = String::new();
    io::stdin().read_line(&mut input).unwrap();
    input.trim().parse::<i32>().unwrap()
}
fn main(){
    let mut t = read_int();
    while t > 0 {
        t -= 1;
        let a = read_int();
        let b = read_int();
        let c = read_int();
        let p = pour1(a, b, c);
        println!("{}", p);
    }
}