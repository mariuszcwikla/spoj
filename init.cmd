@echo off

set /p nn="Enter solution name: "


if "%nn%" == "" goto empty

if exist %nn% goto dir_exist

mkdir %nn%
copy template.rb %nn%\%nn%.rb
touch %nn%\%nn%.in
touch %nn%\%nn%.out

echo "Solution [%nn%] created"

goto end

:dir_exists
echo "Katalog [%nn%] istnieje"
goto end

:empty
echo "Nie podales nazwy katalogu"
goto end

:end
