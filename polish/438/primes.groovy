import java.util.BitSet

def SIZE = 10001
sieve = new BitSet(SIZE);

sieve[0]=true;
sieve[1]=true;		

for(int i=2;i<SIZE/2;i++){
	if(sieve[i]==false){	//optimization
		for(int n=i*2;n<SIZE;n+=i)
			sieve[n]=true;
	}
}

def isPrime(int n){
	return sieve[n]==false
}

def br = new BufferedReader(new InputStreamReader(System.in));

int numOfTests = Integer.valueOf(br.readLine());

for(int i=0;i<numOfTests;i++){
	String line = br.readLine()	
	try{
		int n = Integer.valueOf(line);		
		if(isPrime(n))
			println "TAK"
		else
			println "NIE"
	}catch(NumberFormatException e){
		
	}
}