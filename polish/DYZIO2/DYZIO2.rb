#!ruby

#MAX=10 ** 6 + 1
N = 10 ** 6 + 1
#N = Math.sqrt(MAX)

sieve = Array.new(N)

sieve[0]=true
sieve[1]=true
sieve[2]=false

(2..N).each do |i|
  if !sieve[i]
    k = 2*i
    while k<N do
      sieve[k]=true
      k += i
    end
  end
end

numOfPrimes = Array.new(N)
numOfPrimes[0]=0
numOfPrimes[1]=0
(2..N).each do |i|
  if sieve[i]
	numOfPrimes[i]=numOfPrimes[i-1]
  else
    numOfPrimes[i]=numOfPrimes[i-1]+1
  end
end

n = gets.to_i
n.times do |i|
  a,b = gets.split
  a=a.to_i
  b=b.to_i
  p1 = numOfPrimes[a-1]
  p2 = numOfPrimes[b]
  puts (p2-p1)
end

