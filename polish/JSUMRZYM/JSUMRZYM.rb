#!ruby

def roman_to_i(n)
	i = 0
	r = 0
	digits = {'I' => 1, 'V' => 5, 'X' => 10, "L" => 50, "C" => 100, "D" => 500, "M" => 1000}
	prev = nil
	v = 0
	while i<n.length		
		d = digits[n[i]]		
		if prev == d or prev==nil
			v+=d
		elsif prev < d
			r+=(d - v)
			v=0
		else
			r+=v
			v=d
		end
		prev = d
		i+=1
	end
	r+=v
	return r
end


def i_to_roman(n)

	def step(n, a, b, c)
		return "" if n==0
		if n<=3
			return a * n
		elsif n==4
			return a+b
		elsif n==5
			return b
		elsif n<=8
			return b + a * (n-5)
		else
			return a + c
		end
	end

	s = step(n%10, "I", "V", "X")
	n/=10
	s = step(n%10, "X", "L", "C") + s
	n/=10
	s = step(n%10, "C", "D", "M") + s
	n/=10
	s = step(n%10, "M", "", "") + s
	
	return s
	
end

#(1..1000).each do |x| puts "#{x} -> #{i_to_roman(x)}" end

while $stdin.eof==false
	line = readline
	a,b = line.split
	a = roman_to_i(a.strip)
	b = roman_to_i(b.strip)
	#puts a+b
	puts i_to_roman(a+b)
end
