#!ruby

def modexp(a,b,n)
	return 1 if b==0
	return a if b==1
	exponents = { 0 => 1, 1=>a }    
	i = 2    
	squared = a
	while i<=b
	  squared = squared * squared % n
	  exponents[i] = squared
	  i*=2
	end

	exponent = i/2
	v = exponents[exponent]

	add_to_exponent = exponent/2 
	while exponent <b
	  next_exponent = exponent + add_to_exponent 
	  if next_exponent == b
		return (v * exponents[add_to_exponent]) % n
	  elsif next_exponent > b
		  add_to_exponent/=2
	  else
		  v= (v * exponents[add_to_exponent]) % n      
		  exponent = next_exponent
	  end
	end

	raise "current!=b: #{exponent} <-> #{b}" unless exponent==b
	return v
end

require 'scanf'

t = gets.to_i
t.times do |i|
  a,b,c = gets.scanf("%d%d%d")
  puts modexp(a,b,c)
end

