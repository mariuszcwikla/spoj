#!ruby

def gray(n,tab,k,dir)
	if n==k
		puts tab
	else
		if dir==1
			tab[k]='0'
		else
			tab[k]='1'
		end
		gray(n,tab,k+1, 1)
		if dir==1
			tab[k]='1'
		else
			tab[k]='0'
		end
		gray(n,tab,k+1, 0)
	end
end

n = gets.to_i
n.times do |i|
  a = gets.to_i
  #gray(a, "x"*a, 0, 0) 
  gray(a, "x"*a, 0, 1)
  puts ""
end

