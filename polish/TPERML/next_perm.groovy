/*
krok 1		wyszzykujemy od tyłu takie gdzie t[i-1]<t[i] i zamieniamy miejscami
krok 2		reverse

7135624			
...5642			krok 1: 2-4		
...6245			krok 1: 5-6			6542
				krok 2: 			.245
				
...6245			krok 2 robimy reverse 542 na 245
   
...6254			krok 1 5<->4
   
...6524			krok 1 5<->2

Drugi sposób - badam zmienność ciągu.
0 oznacza malejący ciąg, 1 rosnący. Pierwsza cyfra jest nieistotna

7135624			x011101
7135642			x011100
7136245         x011011
7136254         x011010

..niestety nie zauwazylem tutaj wzorca - ale kolejny sposób czerpie z tego pomysłu


Trzeci sposób: niezawodna metoda "dziel i rządź"
Wezmy przykladowo 7136245
Ostatnie 4 cyfry mają x011
Jeśli przedstawimy całą liczbę w formie binarnej jak w przykladzie, to wyszukujemy "od tyłu" najdłuższy ciąg o samych jedynkach.
I ten będzie permutowany, czyli wystarczy zamienić 4 i 5 (bo jest x011)
Kolejna liczba:

7136254			x011010

niestety to też nie działa, ale idziemy dalej z pomysłem.



Czwarty sposób:

- Ale mamy kolejną zauważoną właściwość: jeśli jest "11" to wystarczy zamienić miejscami dwie cyfry *wynika z poprzedniego).

Co zrobić z przypadkiem gdy jest 010?
Musi powstać x001 (425).
Następnie jest 452 (x010)

Dalej czegos nie widzę...

Piąty sposób - spróbujmy wydzielić dwie grupy, tak aby zastosować "sdziel i rządź". Grupa po prawej to będzie ten podzbiór który będziemy permutować.
Napewno coś z pierwszą jedynką/pierwszymi jedynkami będzie wspólnego.

7135624	  71356 24	   x0111 01
7135642	  713 5642	   x01 1100
7136245   71362 45     x0110 11
7136254   7136 254     x011 010


Szósty pomysł:
Idziemy od tyłu. Dobieramy kolejne cyfry i będziemy odpowiadać czy istnieje następna permutacja (odpowiednik - funkcja "hasNextPerm").
Wychodzimy od 7135642

42 - NIE    (x0)
642 - NIE   (x00)
5642 - TAK  (x100)




Dla 1243
43 - NIE  (x0)
243 - TAK (x10)
Wyszukujemy najmniejszą liczbę większą od liczby na miejscu "x", czyli 3. To będzie nowy początek ciągu.

3xx
Czy wystarczy tutaj zrobić "zwykły shift" trójki na poczatek?
Dla 5642 -> 6542. Reverse z 542?




*/

//tab = [4,1,3,5,2,6]
tab = [1,2,3,4]

def print_tab(int digits){
	def sb = new StringBuffer()
	for(int i=0;i<digits;i++)
	{
		//print tab[i] + ' '
		sb.append(tab[i])
		sb.append(' ')
	}
	
	sb.append("\n")	 
	print sb.toString()
}
/**
* Dla ciągu 7135642 - chcemy 6-ke przeniesc na miescje 5
* @param digit 
* @param i
*/
def bumpLeft(int from, int to){
	def v = tab[from]	
	for(int i=from; i > to-1;i--)
		tab[i]=tab[i-1]
	tab[to]=v
}

def permute_current(int n){
	
	
	for(int i=n-1;i>0;i--){
		
		if(tab[i-1]<tab[i]){
			//tab[i-1]=5
			//tab[i]  =6
			//np. 5642 - trzeba znalezc pierwszą wiekszą liczbę niż 5
			
			//1243 - trzeba znalezc 3 i trójke wybumpować
			int minIndex = i
			for(int j=i;j<n;j++)
				//if   6  > 5        && tab[j] < min
				if(tab[j] > tab[i-1] && tab[j] < tab[minIndex])
					minIndex=j;
						
			bumpLeft(minIndex, i-1)
			
			//tab.swap(i-1, i)
			
			//6542
			//Arrays.sort(tab, i, n)
			//Collections.sort(tab.subList(i, n))
			//Collections.reverse(tab.subList(i, n))
			break
		}
	}
}
print "0: "
print_tab(tab.size())

10.times {	
	permute_current(tab.size()); 
	print "${it+1}: "
	print_tab(tab.size());
}
 
