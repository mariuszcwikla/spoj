/*
Uwaga: ten program wymaga zmiany. n moze byc z przedzialu [2, 100]. 100! ma wartośc ok 9.3*10^157,
wiec nie zmiesci sie to w liczbie int. Nalezy sprawdzic czy zmiana na BigDecimal jest wystarczająca czy 
jeszcze jest wymagana dalsza zmiana algorytmu.

Chodzi tak naprawdę o trzy operacje:

		 def f = factorial(digits-1);
		 int digit = (index / f) + 1;
		
		oraz
		
			index % f
			
ktore nalezaloby uwzglednic


Obserwacje

Dla N=3, N!=6

 123
 132
 213
 231
 312
 321
 
 
Podejscie 1 - badanie roznicy miedzy dwoma kolejnymi
Dla N=4, N!=24
 
                                        roznica
 1 1234          n=4, i=1, pos=4  		
 2 1243		     n=4, i=1, pos=3		+9
											 
 3 1324			 n=3, 					+81
 4 1342			 n=3,					+18
 
 5 1423									+81
 6 1432									+9
 
 7 2134									+702=9*78=2*9*39
 8 2143									+9
 9 2314									+171=9*19
 10 2341							    +27
 
Prawdpodobnie tutaj będzie tak: +39, +9, +171, +27, +171, + 9, +39
 
 
Podejscie 2

  N=4.
  
 1234               1
 1243
 1324
 1423
 1432               6
 2134               7
 2143
 2314
 2341
 2413
 2431               12
 3124
 ....
 3421               18
 4123               19
 ...
 
 (N-1)! = 6 
 Zatem 1xxx - 6 wariantów, 2xxx - 6 wariantów, itd.
 
 Przyklad: index=10.
 index/(N-1)! +1 = 2 - cyfra wiodąca.
 
 Więc bierzemy "2", "wypychamy" ją na początek, dzielimy 4 cyfry na 1+3 cyfry i rekurencyjnie pracujemy na pozostalej czesci tj:
 
 2 134
 
 Teraz nalezy spermutować 134. Nowe N=3, N!=6, (N-1)!=2. Dla kazdej cyfry wiodącej jest po 2 warianty.
 
 
 Zatem wychodzi nam algorytm "Dziel i rządź" (programowanie dynamiczne)
 */
 
 
 
 def factorial(int n){
	/*
	Mozliwa optymalizacja: scache'ować wartosc silnii
	*/
	 int k = 1;
	 for(int i=2;i<=n;i++){
		 k = k*i;
	 }
	 return k;
 
 }
 
 def swap(int []tab, int a, int b){
	 int c = tab[a];
	 tab[a]=tab[b];
	 tab[b]=c;
 }
 
 def bump(int [] tab, int digit, int i){
	 for(int k=digit+i; k>i;k--)
		 swap(tab, k, k-1)
 }
 
 def perm_recursive(int digits, int index, int [] tab, int i){
	 if(digits==1)
		 return;
		 //print '1';
	 else{
		 def f = factorial(digits-1);
		 int digit = (index / f) + 1;
		 //swap(tab, i, digit-1)
		 bump(tab, digit-1, i)
		 //print digit
		 perm_recursive(digits-1, index % f, tab, i+1);
	 }
 }
 
 def perm(int digits, int index){
	 int [] tab = new int[digits];
	 for(int i=1;i<=digits;i++)
		 tab[i-1]=i;
	 perm_recursive(digits, index, tab, 0);
	 for(int i=0;i<digits;i++)
		 print tab[i] + ' '
		 
	 println ""
 }
 
 
 def sc = new Scanner(System.in);
 
 def numOfTests = sc.nextInt()
 
 for(int i=0;i<numOfTests;i++){
 
	int n = sc.nextInt();
	int index = sc.nextInt();
	int m = sc.nextInt();
	
	//zamiast tej petli moznaby zoptyamlizowac i:
	//1. obliczyc pierwszą permutację raz
	//2. kolejne permutacje na podstawie poprzedniej
	//taki algorytm pewnie bedzie szybszy niz oryginalny
	for(int j=0;j<m;j++){
		perm(n, index + j);		
	}
	println ""
 }