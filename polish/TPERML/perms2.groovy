/*

Dalsza optymalizacja, bo o ile taki test dziala szybko (0.059 sek) (tj. znalezeienie pierwszej permutacji)

100 4238748923749238 1

to znalezienie kolejnych 100

100 4238748923749238 1

dziala ok 0.5 sekundy.

Zatem dalsza optymalizacja bedzie polegac na wprowadzeniu algorytmu ktory znajduje "kolejną" permutację w stosunku do biezacej.
To powinno byc szybsze niż ciągłe operacje na BigInteger.

Przyklad - mamy permutację

3214		

Znajdujemy pierwszą liczbę gdzie t[i]<t[i+1] - mamy dla pary 1,4, więc zamieniamy t[i] i t[i+1] (i=2)

3241

Następna permutacja - tak samo (i=1)

3421, ale to nie jest ok, bo teraz trzeba wypchać dwójkę na koniec, to jest (bumpRight):
3412

Sprawdzamy czy algorytm dalej działa dobrze. Kolejny krok:

3421		t[2] i t[3]
4321->4213		t[0] i t[1], ale trzeba jeszcze wypchać na koniec. Niestety nie wystarczy.
Jednak trzeba "posortować elementy t[1]...t[3], czyli:
4123


*/

factorials = new BigInteger[101];
factorials[0]=BigInteger.valueOf(1);
factorials[1]=BigInteger.valueOf(1);
for(int i=2;i<=100;i++)
	factorials[i]=factorials[i-1] * i;


 def bump(int [] tab, int digit, int i){
	 for(int k=digit+i; k>i;k--)
		 tab.swap(k, k-1)
 }
 def perm_recursive(int digits, BigInteger index, int [] tab, int i){
	 if(digits==1)
		 return;
	 else{
		 def f = factorials[digits-1]
		 int digit = (index / f) + 1;
		 bump(tab, digit-1, i)
		 perm_recursive(digits-1, index % f, tab, i+1);
	 }
 }
 
 tab = new int[100];
 def perm(int digits, BigInteger index){
	
	 for(int i=1;i<=digits;i++)
		 tab[i-1]=i;
				  
	 perm_recursive(digits, index, tab, 0);
	 def sb = new StringBuffer()
	 for(int i=0;i<digits;i++)
		 //print tab[i] + ' '
	{
		sb.append(tab[i])
		sb.append(' ')
	}
	
	sb.append("\n")	 
	print sb.toString()
 }
 
 
 def sc = new Scanner(System.in);
 
 def numOfTests = sc.nextInt()
 
 long beg = System.currentTimeMillis()
 
 for(int i=0;i<numOfTests;i++){
	
	int n = sc.nextInt();
	BigInteger index = sc.nextBigInteger();
	int m = sc.nextInt();
	

	for(int j=0;j<m;j++){
	
		def index_plus_j = (index + j) % factorials[n];		

	//	print "${index_plus_j}: "
		perm(n, index_plus_j);
	}
	println ""
	
 }
 
//System.err.println("total time: "  + ( (System.currentTimeMillis() - beg) / 1000 ) + "ms")