
factorials = new BigInteger[101]; 
factorials[0]=BigInteger.valueOf(1);
factorials[1]=BigInteger.valueOf(1);
for(int i=2;i<=100;i++)
	factorials[i]=factorials[i-1] * i;


 def bump(int [] tab, int digit, int i){
	 for(int k=digit+i; k>i;k--)
		 tab.swap(k, k-1)
 }
 def perm_recursive(int digits, BigInteger index, int [] tab, int i){
	 if(digits==1)
		 return;
	 else{
		 def f = factorials[digits-1]
		 int digit = (index / f) + 1;		 
		 bump(tab, digit-1, i)		 
		 perm_recursive(digits-1, index % f, tab, i+1);
	 }
 }
 
 def print_tab(int digits){
	def sb = new StringBuffer()
	for(int i=0;i<digits;i++)
	{
		//print tab[i] + ' '
		sb.append(tab[i])
		sb.append(' ')
	}
	
	sb.append("\n")	 
	print sb.toString()
}

tab = new int[100];
 def perm(int digits, int index){
	
	for(int i=1;i<=digits;i++)				 
		 tab[i-1]=i;
		 		 
	perm_recursive(digits, index, tab, 0);			 
 }
 
 
 
 def permute_current(int n){
	
	
	for(int i=n-1;i>0;i--){
		
		if(tab[i-1]<tab[i]){
			//tab[i-1]=5
			//tab[i]  =6
			//np. 5642 - trzeba znalezc pierwszą wiekszą liczbę niż 5
			
			//1243 - trzeba znalezc 3 i trójke wybumpować
			int minIndex = i
			for(int j=i;j<n;j++)
				//if   6  > 5        && tab[j] < min
				if(tab[j] > tab[i-1] && tab[j] < tab[minIndex])
					minIndex=j;
						
			//bumpLeft(minIndex, i-1)
			tab.swap(minIndex, i-1)
			//Arrays.sort(tab, i, n)
			
			//tab.swap(i-1, i)
			
			//6542
			Arrays.sort(tab, i, n)
			//Collections.sort(tab.subList(i, n))
			//Collections.reverse(tab.subList(i, n))
			break
		}
	}
}
 
 
 def sc = new Scanner(System.in);
 
 def numOfTests = sc.nextInt()
 
 for(int i=0;i<numOfTests;i++){
	
	int n = sc.nextInt();
	BigInteger index = sc.nextBigInteger();
	int m = sc.nextInt();
	
	perm(n, index);		
	print_tab(n)
	for(int j=1;j<m;j++){
		//perm(n, index + j);		
		permute_current(n)
		
		print_tab(n)
	}
	println ""
	
 }