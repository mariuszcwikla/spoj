
print "   "
(1..20).each do |i|
  print "%3d" % i
end
puts
print "  "
print "---" * 20
puts ""

(1..40).each do |a|
  print "%3d" % a
  (1..40).each do |b|
    d = (a**b) % 10
    print "%3d" % d     
  end
  puts
end