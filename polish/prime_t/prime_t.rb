

MAX=100000
N = Math.sqrt(MAX)

sieve = Array.new(N)

sieve[0]=true
sieve[1]=true
sieve[2]=false

(2..N).each do |i|
  if !sieve[i]
    k = 2*i
    while k<N do
      sieve[k]=true
      k += i
    end
  end
end

n = $stdin.readline.to_i
n.times do |i|
  v = $stdin.readline.to_i
  
  unless sieve[v] 
    puts 'TAK'
  else
    puts 'NIE'
  end
end
