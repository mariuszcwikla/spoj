
#TODO do usuniecia??? Jest już "spoj.rb"

=begin
Dir.foreach('./') do |fname|
  if fname.end_with('.rb') and 
end
=end

#TODO sprobowac uzyc biblitoeki "diff"

def matchLines(solution, expected, result)
  i = 0
  j = 0
  while i<expected.length and j<result.length
    
    while i<expected.length 
      break unless expected.empty?
      i+=1
    end
    while j<result.length 
      break unless result.empty?
      j+=1
    end
    
    e = expected[i].strip
    r = result[j].strip
    
    if e!=r
      puts "[#{solution}]: FAILED"
      puts "\texpected (#{i}): " + e
      puts "\t but was (#{j}): " + r
      return
    end   
    
    i+=1
    j+=1 
  end
  
  while i<expected.length 
      break unless expected.empty?
      i+=1
  end
  while j<result.length 
      break unless result.empty?
      j+=1
  end
  
  if i!=expected.length
    puts "[#{solution}]: TOO SHORT input. Missing lines:"
    puts expected[i..-1]
    return
  end
  if j!=result.length
    puts "[#{solution}]: TOO LONG input. Extraneous lines:"
    puts result[j..-1]
    return
  end  
  
  puts "[#{solution}]: OK"
end

def run(solution)
  base_name = solution + '/' + solution
  if File.exists?(base_name + ".rb")
    unless File.exists?(base_name + ".in")
      puts "WARN: #{solution}.in  does not exist"
      return
    end
    
    unless File.exists?(base_name + ".out")
      puts "WARN: #{solution}.out does not exist" 
      return
    end
    
    result = `ruby #{base_name}.rb
    < #{base_name}.in 2>&1`.lines
    
    expected = IO.readlines(base_name + ".out")
    
    matchLines(solution, expected, result)
  end
end

[
  "prime_t",
  "fctrl3",
  "pa05_pot",
  "flamaste"
].each do |s| run (s) end

  
  