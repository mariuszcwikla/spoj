#!/usr/bin/env ruby

# dodaj do .bashrc
# alias spoj="ruby ~/spoj/spoj.rb"

=begin

gem install diffy
cp spoj.rb ~/.spoj.rb
echo "alias spoj=\"ruby ~/.spoj.rb\"" >> $HOME/.bashrc

=end

#gem install diffy

require 'getoptlong'
require 'diffy'

def printHelp()

	puts <<EOF
spoj [OPTION] [ARGUMENTS]

  -h, --help:
    show help

  --init [name], -i [name]:
    create a directory "name" and files test.rb test.in test.out.
  
  --addtest [test], -a [test]
    create test.in/test.out files
  
  --test [name], -t [name]
    Runs test; "name" is optional:
    - if "name" is provided - runs test in directory "name"
    - if "name" is not provided - rusn test in current directory 
	
  --java
    create java template instead of ruby
  
  If no argument is given, runs test in current directory.	
  Examples:
  
	spoj -i prime_t
	spoj -i 439
	spoj -i 439 --java

	spoj -t
	spoj -t prime_t
	spoj prime_t
	spoj
	
  
EOF

end


mode_help=false
mode_init=false
mode_add_testcase=false
mode_test=false
$lang_template = :ruby

testname=nil


opts = GetoptLong.new(
  [ '--help', '-h', GetoptLong::NO_ARGUMENT ],
  [ '--init', '-i', GetoptLong::REQUIRED_ARGUMENT ],
  [ '--addtest', '-a', GetoptLong::OPTIONAL_ARGUMENT ],
  [ '--test', '-t', GetoptLong::OPTIONAL_ARGUMENT],
  [ '--java', '-j', GetoptLong::NO_ARGUMENT ]
)
#opts.quiet = true		#

begin
opts.each do |opt, arg|

  case opt
    when '--help'	
	  mode_help=true
	when '--init'
	  mode_init=true
	  testname = arg
	when '--addtest'
	  mode_add_testcase=true
	  testname=arg	  	
	when '--test'
	  mode_test=true
	  testname = arg
	when '--java'	  
	  $lang_template = :java
  end
end
rescue Exception
  printHelp
  exit 0
end

if mode_help
	printHelp
	exit 0
end

if mode_init and mode_add_testcase and mode_test
	puts "Too many arguments"
	printHelp
	exit 0
end


$yes_to_all=false

def check_file_existence(f)
	return true if $yes_to_all
	if File.exist?(f)
		puts "File #{f} exists. Overwrite? [y/n/a]: "
		a = gets.strip.downcase
		if a=='y'
		  return true
		elsif a=='a'
		  $yes_to_all=true
		  return true		
		else
		  return false
		end
	end
	return true
end


def create_test_case(infile, outfile)
		File.open(infile, 'w') do |f| 
            f.puts <<~EOF
                    2
                    10
                    20
                    EOF
            end

		File.open(outfile, 'w') do |f| 
            f.puts <<~EOF
                    20
                    40
                    EOF
            end

end

RUBY_TEMPLATE=<<EOF
#!ruby

t = gets.to_i
t.times do |i|
  a = gets.to_i
  puts a*2
end

EOF

JAVA_TEMPLATE=<<EOF
import java.util.*;

public class Main{
  public static void main(String []args){
	Scanner in = new Scanner(System.in);
	String line = in.nextLine();
	int numOfTests = Integer.valueOf(line);
	while(numOfTests-->0) {
		line = in.next();
		int a = Integer.valueOf(line);
		System.out.println(a*2);
	}
  }
}
EOF

def init(name)
	if Dir.exists?(name)
	  puts "Warning: folder #{name} exists"
	else
	  Dir.mkdir(name)
	end
	template = RUBY_TEMPLATE	
	basename = name + "/" + name	
	programname = name + "/main.rb"
	
	if $lang_template == :java
		template = JAVA_TEMPLATE 		
		programname = name + "/" + "Main.java"
	end
	
	
	infile = basename + ".in"
	outfile = basename + ".out"	
	
	
	if check_file_existence(programname) and check_file_existence(infile) and check_file_existence(outfile)
				
		File.open(programname, 'w')	do |f| f.puts template	end

		create_test_case(infile, outfile)

		puts "SPOJ template created"
		
		puts " > cd #{name} "
		
#		Dir.chdir (name) do
#			system "vim -p *"
#		end
		
		
	else
		puts 'Test cases not created'
	end
	  
end



def add_test_case(testname)
	
	infile = testname + ".in"
	outfile = testname + ".out"
	
	if check_file_existence(infile) and check_file_existence(outfile)
		create_test_case(infile, outfile)
		puts "Test case (#{infile}, #{outfile}) created"
	else	
		puts "Test cases not created"
	end
end

def compare_result(fname, result)
	f = File.open(fname)
	actual = f.read
	f.close
	
	d = Diffy::Diff.new(actual, result)	
	if d.to_s.strip.empty?
		puts "PASSED"
		puts ""
		return true
	else
		puts "FAILED:"
		puts d 
		return false
	end
end

def run_test(testname)
	subdir=false
	if testname==nil or testname==""
		name = File.basename(Dir.getwd)
	else		
		if Dir.exists?(testname)
			name = testname
			subdir=true
		else
			puts "Directory #{name} does not exist!"
			return
		end
	end
	def check(name)
		if File.exist?	(name)
			return name
		elsif File.exist? 'main.rb'
			return 'main.rb'
		else
			raise "FAILURE: File #{name} does not exist"
		end
	end
	
	
	def check_and_run(name)
		
		name = check(name +".rb") 
		
		testcases = []
		Dir.glob("*.in") { |filename|
			basefilename = File.basename(filename, ".in")			
			if File.exists?(basefilename + ".out")
				testcases << [filename, basefilename + ".out"]
			end
		}
		
		
		
		if testcases.empty?
			puts "No test cases found. Exiting..."
		else
			passed = 0
			i = 1
			testcases.each { |pair|
				
				puts "Test \##{i}: #{pair[0]} - #{pair[1]}"
				i+=1
				result = `ruby #{name} <#{pair[0]}`			
				puts 
				puts "Output:"
				puts result
				puts "--------------------------------"
				passed += 1 if compare_result(pair[1], result)
			}
			puts "Total: #{passed}/#{testcases.length}"
		end
	end
	
	if subdir
		Dir.chdir(name){
			check_and_run(name)
		}
	else
		check_and_run(name)
	end	
end

if mode_init
	init(testname)
elsif mode_add_testcase
	add_test_case(testname)
elsif mode_test
	run_test(testname)
else
	#uruchomiono "spoj fctrl"
	if ARGV.length==1
		run_test(ARGV[0])
	elsif ARGV.length==0
		run_test(nil)		
	else
		printHelp
	end
end

