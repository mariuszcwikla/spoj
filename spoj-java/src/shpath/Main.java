package shpath;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Scanner;
import java.util.TreeSet;

class Adjacency{
	public Adjacency(int nr, int cost) {
		this.cityNo = nr;
		this.cost = cost;
	}
	int cityNo;
	int cost;
}

class City implements Comparable<City>{

	public int index;
	public String cityName;
	public List<Adjacency> adjacent = new ArrayList<>();
	public int distance;
	public City(String cityName, int index) {
		this.cityName = cityName;
		this.index = index;
	}

	public void addAdjacent(int nr, int cost) {
		adjacent.add(new Adjacency(nr, cost));
	}
	@Override
	public int compareTo(City o) {
		int d = distance - o.distance;
		if (d==0)
			return index - o.index;
		return d;
	}
		
}

public class Main {

	private static final int INF = 100_000_000;

	private static int shortestPath(City [] cities, int from, int to) {
		
		PriorityQueue<City> q = new PriorityQueue<>();
		for(int i=1;i<cities.length;i++) {
			City city = cities[i];
			if (city.index == from)
				city.distance = 0;
			else
				city.distance = INF;
		}
		q.add(cities[from]);
		
		while (!q.isEmpty()) {
			City city = q.remove();
			if (city.index == to)
				break;
			city.adjacent.forEach( adj -> {
				int new_dist = city.distance + adj.cost;
				City other = cities[adj.cityNo];
				if (new_dist < other.distance) {
//					q.remove(other);
					other.distance = new_dist;
					q.add(other);
				}
			});
		}
		
		City toCity = cities[to];
		return toCity.distance;
	}


	public static void main(String[] args) {
		Scanner in = new Scanner(new BufferedReader(new InputStreamReader(System.in)));
		int numOfTests = in.nextInt();
		while(numOfTests-->0) {
			int n = in.nextInt();
			
			Map<String, Integer> cityNames = new HashMap<>();
			//Map<Integer, City> cities = new HashMap<>();
			City[] cities = new City[n+1];
			
			for(int i=1;i<=n;i++) {
				City city = new City("", i);
				//cities.put(i, city);
				cities[i] = city;
			}
			
			for(int i=1;i<=n;i++) {
				String cityName = in.next();
				cityNames.put(cityName, i);
				City city = cities[i];
				city.cityName = cityName;
				
				int adjNum = in.nextInt();
				city.adjacent = new ArrayList<>(adjNum);
				for(int j=0; j<adjNum; j++) {
					int nr = in.nextInt();
					int cost = in.nextInt();
					city.addAdjacent(nr, cost);
				}
			}
			
			int tests = in.nextInt();
			while(tests-->0) {
				int from = cityNames.get(in.next());
				int to = cityNames.get(in.next());
				int shortest = shortestPath(cities, from, to);
				System.out.println(shortest);
			}
			in.nextLine();
		}
		in.close();
	}

}
