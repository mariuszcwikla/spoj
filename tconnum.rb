
=begin
n = $stdin.readline.to_i
n.times do |i| 
	nn,r,s = $stdin.readline.split
	r = r.to_i
	s = s.to_i
	v = 0
	
	nn.each_char do |c| 
		if c>='A' and c<='Z'
			x = c.ord - 'A'.ord + 10
		else
			x = c.ord - '0'.ord
		v = v + x * r
	end
	
	s = ''
	while v>0
		x = v mod s
		if x<=9
			digit = '0'.ord + x
		else
			digit = 'A'.ord + (x-10)
		end
		
		digit = digit.chr
		s << digit
		v/=s
	end
	puts s
end
=end
