#!ruby

$prio = {'+' => 1, '-' => 2, '*' => 3, '/' => 4, '^' => 5}


def to_onp(line)
	
	
	opstack = []
	out = ""
	line.strip.each_char do |c|
		next if c =~ /\s/
		if c=='('
			opstack.push(c)
		elsif c==')'
			until opstack.empty? or (cc=opstack.pop)=='('
				out<<cc
			end
		else
			if $prio.include? c				
				until opstack.empty? 
					if opstack.last == '('
						#opstack.pop
						break
					elsif $prio[opstack.last] == $prio[c]
						if c=='-' or c=='/'
							out << opstack.pop
						else
							break
						end
					elsif $prio[opstack.last] < $prio[c]												
						break
					else						
						out << opstack.pop
					end
				end	
				opstack.push c
			else
				out << c
			end
		end
	end	
	
	until opstack.empty?
		out<<opstack.pop
	end
	return out
end

class Expression
	attr_accessor :expr, :operator		
	def with_operator(c, operand_on_left)
		return @expr if @operator==nil       #i.e. represents variable, not an expression with operator
		#return "(" + @expr + ")" if true
		decorate = false
		
		if $prio[c] > $prio[@operator] 
			decorate=true
		#elsif (c=='/' or c=='-') and operand_on_left==false
		#	decorate=true
		#end
		elsif operand_on_left==false
			if c=='/'
				decorate = true			
			elsif c=='-' and (@operator=='/' or @operator=='*')
				decorate = false
			elsif c=='-'
				decorate = true
			end		
		end
		
		return "(" + @expr + ")" if decorate
		return @expr
	end
	
end

def simplify(onp)
	
	stack = []
	
	out = ""
	
	
	onp.each_char do |c|
		if $prio.include? c 
			right = stack.pop
			left = stack.pop
			
			left_expr = left.with_operator(c, true)
			right_expr = right.with_operator(c, false)
			
			e = Expression.new
			e.expr = left_expr + c + right_expr
			e.operator = c
			stack.push (e)
		else
			e = Expression.new
			e.expr = c
			stack.push(e)
		end
	end
	return "" if stack.empty?
	return stack.pop.expr
end

t = STDIN.gets.to_i
t.times do |i|
  a = STDIN.gets.strip
  o = to_onp(a)
  #puts a + "  ...>   " + o
  
  puts a + " ..>  " + o + " ..> " + simplify(o) if ARGV.index("-d")!=nil
  puts simplify(o)
end

