
def gcd(a,b)
  while b!=0
    t = b
    b = a % b
    a = t
  end
  return a
end

def lcm(a,b)
  return a * b / gcd(a,b)
end

n = $stdin.readline.to_i
n.times do |i|
  a,b = $stdin.readline.split
  a = a.to_i
  b = b.to_i
  
  puts lcm(a,b)
  
end
