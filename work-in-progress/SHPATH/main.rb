# Daje Time Limit Exceeded :(

class BinaryHeap
  class Element
    # position wskazuje na indeks w tablicy gdzie znajduje się ten element
    attr_reader :value, :position
    attr_writer :value, :position

    def initialize(value, position)
      @value = value
      @position = position
    end
  end

  attr_reader :elems # for debugging

  def initialize(&block)
    @elems = []
    @comparator =
      if block.nil?
        :<.to_proc
      else
        block
      end
  end

  def empty?
    @elems.empty?
  end

  def size
    @elems.size
  end

  def push(n)
    e = Element.new(n, @elems.size)
    @elems << e
    heapify
    e
  end

  alias << push
  alias insert push

  private def heapify
    i = @elems.size - 1
    heapify_up(i)
  end

  def swap_elements(ia, ib)
    @elems[ia], @elems[ib] = @elems[ib], @elems[ia]
    @elems[ia].position = ia
    @elems[ib].position = ib
  end

  private def heapify_up(i)
    while i > 0
      p = (i - 1) >> 1
      if @comparator.call(@elems[i].value, @elems[p].value)
        swap_elements(i, p)
        i = p
      else
        break
      end
    end
  end

  private def heapify_down(i)
    val = @elems[i].value
    while i * 2 + 1 < @elems.size
      l = i * 2 + 1
      r = i * 2 + 2

      lowest = l
      lowest = r if r < @elems.size && @comparator.call(@elems[r].value, @elems[l].value)
      if @comparator.call(@elems[lowest].value, val)
        swap_elements(i, lowest)
        i = lowest
      else
        return
      end
    end
  end

  def pop
    raise 'heap is empty' if empty?

    if @elems.size == 1
      @elems.pop.value
    else
      e = @elems[0]
      @elems[0] = @elems.pop
      @elems[0].position = 0
      heapify_down(0)
      e.value
    end
  end

  def peek
    raise 'empty heap' if @elems.empty?

    @elems[0].value
  end

  def update_key(e, n)
    if @comparator.call(e.value, n)
      increase_key(e, n)
    elsif @comparator.call(n, e.value)
      decrease_key(e, n)
    end
  end

  def decrease_key(e, n)
    raise 'e is not BinaryHeap::Element' unless e.instance_of? Element
    raise 'new key is not lower' unless @comparator.call(n, e.value)
    if (e.position > @elems.size) || (@elems[e.position] != e)
      raise 'element not part of this list'
    end

    e.value = n
    heapify_up(e.position)
  end

  def key_decreased(e)
    # TODO: w przypadku dijstry mam tak:
    #
    # second.distance = v.distance + e.weight
    # heap.decrease_key(second.heap_handle, second)
    #
    # a to powoduje ze walidacja "if n <= e.value" failuje bo przeciez wartosc zostala zmodyfikowana z zewnątrz

    # TODO: wydmuszka; brakuje testów; powstalo tylko na potrzeby Dijkstry
    raise 'e is not BinaryHeap::Element' unless e.instance_of? Element
    if (e.position > @elems.size) || (@elems[e.position].object_id != e.object_id)
      raise 'element not part of this list'
    end

    heapify_up(e.position)
  end

  def increase_key(e, n)
    raise 'e is not BinaryHeap::Element' unless e.instance_of? Element
    raise 'new key is not greater' if n <= e.value
    if (e.position > @elems.size) || (@elems[e.position] != e)
      raise 'element not part of this list'
    end

    e.value = n
    heapify_down(e.position)
  end
end


=begin

Optimization history in pop method

Initial implementation

  FibonacciHeap - 100000    1.797000   0.000000   1.797000 (  1.795621)
  FibonacciHeap - 1000000  24.031000   0.000000  24.031000 ( 24.039042)

Change #1 - change Hash to Array of arrays of size=(Math.log(size) / Math.log(2)).floor

  FibonacciHeap - 100000    1.375000   0.000000   1.375000 (  1.361470)
  FibonacciHeap - 1000000  18.500000   0.000000  18.500000 ( 18.514749)

Change #2 - change log_2 calculation from Math.log to binary one - even worse

  FibonacciHeap - 100000    1.406000   0.000000   1.406000 (  1.407220)
  FibonacciHeap - 1000000  19.453000   0.000000  19.453000 ( 19.470669)

Change #3 - change log_2 to Math.log2 - no difference. Sometimes lower, sometimes higher. Not noticeable

  FibonacciHeap - 100000    1.344000   0.016000   1.360000 (  1.361300)
  FibonacciHeap - 1000000  18.484000   0.000000  18.484000 ( 18.498306)

Change #2 - change Array of arrays to Array

Tests from another machine

Before optimization

BinaryHeap - 100000       1.421000   0.000000   1.421000 (  1.422231)
BinomialHeap - 100000     0.875000   0.000000   0.875000 (  0.873336)
FibonacciHeap - 100000    1.906000   0.015000   1.921000 (  1.942554)
BinaryHeap - 1000000     19.734000   0.000000  19.734000 ( 19.773593)
BinomialHeap - 1000000   11.563000   0.000000  11.563000 ( 11.563130)
FibonacciHeap - 1000000  25.328000   0.000000  25.328000 ( 25.401512)

After optimization

BinaryHeap - 1000000     19.734000   0.000000  19.734000 ( 19.752215)
BinomialHeap - 1000000   11.593000   0.000000  11.593000 ( 11.602985)
FibonacciHeap - 1000000  17.375000   0.000000  17.375000 ( 17.375860)

=end

class FibonacciHeap
  class Sentinel
    attr_accessor :prev, :next
    alias tail next
    alias head prev
    def initialize
      @prev = self
      @next = self
    end

    def each_sibling
      n = self.next
      # TODO: czy tu nie brakuje:
      # yield n
      # ?
      while n != self
        current = n
        n = n.next
        yield current
      end
    end

    def find_min
      min = self.next
      each_sibling { |n| min = n if n.value < min.value }
      return nil if min == self

      min
    end
  end

  class Element
    attr_reader :value
    attr_accessor :degree, :parent, :next, :prev, :marked, :first_child, :debug_id

    def initialize(v)
      @value = v
      @first_child = nil
      @parent = nil
      @next = self
      @prev = self
      @degree = 0
      @marked = false
    end

    def is_root?
      @parent.nil?
    end

    def inspect
      "#{@value}(degree=#{degree})"
    end

    def update_value(n)
      @value = n
    end

    def merge_list(e)
      # merge 2 lists - insert list after self:

      #   self <-> next
      #
      #   self <-> e <-> .... <-> e.prev <-> next

      @next.prev = e.prev
      e.prev.next = @next
      e.prev = self
      @next = e
    end

    def find_non_sentinel(node)
      if node.instance_of? Sentinel
        if node.next.instance_of? Sentinel
          return nil
        else
          return node.next
        end
      end
      node
    end

    def disconnect_node
      # Tutaj Sentinel sprawia troche problemow, bo
      # parent.first_child = @next
      # powodowalo ze pojawial sie Sentinel w first_child

      if @parent&.first_child == self
        @parent.first_child = find_non_sentinel(@next)
        if @parent.first_child == self
          @parent.first_child = nil
        end
        max_degree = 0
        @parent.first_child&.each_sibling { |n| max_degree = n.degree if n.degree > max_degree }
        @parent.degree = max_degree + 1
      end
      @parent = nil
      @prev.next = @next
      @next.prev = @prev
      @next = @prev = self
    end

    def append_child(n)
      raise 'append_child(self)' if n == self

      n.parent = self
      if @first_child.nil?
        @first_child = n
      else
        @first_child.merge_list(n)
      end
    end

    def to_s
      "#{@value} (#{@degree}, id=#{@debug_id})"
    end

    def siblings_to_s
      s = ''
      n = 10
      each_sibling do |e|
        p = parent.nil? ? '' : parent.value
        s << "#{e.value} (#{e.degree}, id=#{e.debug_id}, p=#{p}) "
        n -= 1
        if n <= 0
          s << '...'
          return s
        end
      end
      s
    end

    def each_sibling(&block)
      n = self.next
      # TODO: czy tu nie brakuje:
      # yield self
      block.call self
      # ?
      while n != self
        current = n
        n = n.next
        # yield current unless current.instance_of? Sentinel
        block.call(current) unless current.instance_of? Sentinel
      end
    end

    def each_child(&block)
      first_child&.each_sibling(&block)
    end
  end

  attr_reader :size, :min

  def initialize
    @roots = Sentinel.new
    @min = nil
    @size = 0
    @next_debug_id = 0
  end

  def empty?
    @min.nil?
  end

  def push(value)
    @size += 1

    elem = Element.new(value)
    elem.debug_id = @next_debug_id
    @next_debug_id += 1
    if @min.nil?
      @min = elem
      @min.merge_list(@roots)
    else
      @min.merge_list(elem)
      if value < @min.value
        @min = elem
      end
    end
    elem
  end

  alias << push

  def peek
    raise 'empty heap' if @min.nil?

    @min.value
  end

  def pop
    raise 'empty heap' if @min.nil?

    @size -= 1
    min_v = @min.value

    if size == 0
      @min = nil
      @roots = Sentinel.new
    else
      unless @min.first_child.nil?
        @min.first_child.each_sibling { |c| c.parent = nil }
        @min.first_child.merge_list(@min.prev)
      end

      @min.disconnect_node

      method = 1

      # r = []
      # @roots.each{|e| r << e}

      # root = @roots.find{|x| x!=nil}

      if method == 1
        max = Math.log2(size).floor
        roots = Array.new(max + 1)

        steps = 0

        @roots.each_sibling do |e|
          until roots[e.degree].nil?
            a = e
            b = roots[e.degree]
            roots[e.degree] = nil
            a, b = b, a if b.value < a.value
            a.degree += 1
            b.parent = a
            b.disconnect_node
            a.append_child(b)

            e = a
            steps += 1
          end
          roots[e.degree] = e
        end
      else
        # :nocov:
        # excluded from code coverage. Probably I should have remove it;)

        #      all_roots = Hash.new{|h, k|h[k]=[]}
        #      @min.each_sibling{|n| all_roots[n.degree]<<n}
        #      max = all_roots.keys.max

        # max = (Math.log(size) / Math.log(2)).floor
        max = Math.log2(size).floor
        all_roots = Array.new(max + 1) { [] }
        @roots.each_sibling { |n| all_roots[n.degree] << n }

        root = nil

        (0..max).each do |d|
          roots = all_roots[d]
          while roots.size > 2
            a = roots.shift
            b = roots.shift

            a, b = b, a if b.value < a.value
            a.degree += 1
            b.parent = a
            b.disconnect_node
            a.append_child(b)

            all_roots[a.degree] << a
          end
          root = roots[0] if root.nil?
          d += 1
        end
        # :nocov:
      end
      @min = @roots.find_min # phase 2 as per wikipedia
    end
    min_v
  end

  def decrease_key(e, n)
    raise 'e is not FibonacciHeap::Element' unless e.instance_of? Element
    raise 'new key is not lower' if n >= e.value

    e.update_value n
    @min = e if e.value < @min.value

    if !e.parent.nil? && e.value < e.parent.value
      parent = e.parent
      e.disconnect_node
      e.merge_list(@roots)
      unless parent.is_root?
        parent.marked = true
        while !parent.parent.is_root? && parent&.marked
          p = parent.parent
          parent.disconnect_node
          parent.marked = false
          parent.merge_list(@roots)
          parent = p
        end
      end
    end
  end

  def key_decreased(e)
    # TODO: wydmuszka; brakuje testów; powstalo tylko na potrzeby Dijkstry
    while !e.parent.nil? and e.value < e.parent.value
      pull_up(e)
    end
  end

  def increase_key(e, n)
    raise 'e is not BinomialHeap::Element' unless e.instance_of? Element
    raise 'new key is not greater' if n <= e.value

    raise 'not implemented'
  end
end


class City
  include Comparable
  attr_accessor :city_index, :distance
  def initialize(city_index, distance)
    @city_index = city_index
	@distance = distance
  end

  def <=>(other)
    @distance <=> other.distance
  end
end

def shortest_path(g, from, to)
  q = FibonacciHeap.new
  
  cities = {}
  g.each_key do |k|
    city = if k == from
			  City.new(k, 0)
			else
			  City.new(k, 10**10)
			end

    handle = q.push city
	cities[k] = handle
  end
  
  while !q.empty?
    city = q.pop
	g[city.city_index].each do |other_city, dist|
	  handle = cities[other_city]
	  new_dist = city.distance + dist
	  if new_dist < handle.value.distance
		#handle.value.distance = new_dist
	    #q.key_decreased(handle)
		q.decrease_key(handle, City.new(handle.value.city_index, new_dist))
	  end
	end
  end
  cities[to].value.distance
end


t = STDIN.gets.to_i
t.times do |i|
  n = STDIN.gets.to_i
  city_indexes = {}
  graph = {}
  n.times do |j|
	city = gets.chomp
	city_indexes[city] = j+1
	p = gets.to_i
	graph[j+1]={}
	p.times do |pi|
	  nr, cost, _ = gets.split.map(&:to_i)
	  graph[j+1][nr]=cost
	end
  end
  
  r = gets.to_i
  r.times do |ri|
	from, to, _ = gets.split
	from = city_indexes[from]
	to = city_indexes[to]
	
	puts shortest_path(graph, from, to)
  end
  gets
end

