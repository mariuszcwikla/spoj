#!ruby
require 'scanf'

#sieve1 = build_sieve(N)

$debug = ARGV[0]=='-d'

def num_of_factors(value)
	factors = []
	while value > 1 && value % 2 == 0
		factors << 2
		value>>=1
	end
	
	p = 3	
	max = Math.sqrt(value)
	while value > 1 && p < max
		while value % p == 0 && p < max
			factors << p
			value/=p
		end
		p+=2
	end
	factors << value if value > 2
	puts "#{factors.join ','}" if $debug
	factors.uniq.size 
end

t = STDIN.gets.to_i

t.times do |i|
  a = STDIN.gets.to_i
  if num_of_factors(a) % 2 == 1
	STDOUT.puts 'YES'
  else
	STDOUT.puts 'NO'
  end
end

