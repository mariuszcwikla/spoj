#!ruby
require 'prime'

def num_of_factors(value)
	value.prime_division.length
end

t = STDIN.gets.to_i

t.times do |i|
  a = STDIN.gets.to_i
  if num_of_factors(a) % 2 == 1
	STDOUT.puts 'YES'
  else
	STDOUT.puts 'NO'
  end
end

