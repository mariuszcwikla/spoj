#$stdin = File.new('TDBFS/TDBFS.in', 'r')


class Graph
  
  class Vertex
    attr_reader :edges
    def initialize(n)
      @n=n
      @edges=[]
    end
    def addedge(b)
      @edges<<b
    end
  end
  
  def initialize(n)
    @vertices = Array.new(n) {|i| Vertex.new(i)}
  end
  
  def vertex(a)
    @vertices[a]
  end
  
  def dfs(initial)
    visited = Array.new(@vertices.length, false)    
     
    vertexlist = []
     
    st = []
    st  << initial
    #vertexlist << initial
    
     
    until st.empty?      
       vi = st.pop
       unless visited[vi]
        vertexlist << vi
        visited[vi]=true       
        v = @vertices[vi]
        v.edges.each do |b|
          unless visited[b]                      
            st.push b      
          end
        end
        end
     end
     return vertexlist
  end
  
  def bfs(initial)
    visited = Array.new(@vertices.length, false)    
    
    vertexlist = []
    
    q = []
    q  << initial
    vertexlist << initial
    visited[initial]=true
    
    until q.empty?      
      vi = q.shift
            
      v = @vertices[vi]
      
      v.edges.each do |b|
        unless visited[b]
          vertexlist << b
          visited[b]=true 
          q << b
        end
      end            
    end
    return vertexlist
  end
end

t = $stdin.readline.to_i
t.times do |graph_number|
  n = $stdin.readline.to_i
  g = Graph.new(n)
  n.times do |vertex_number|
    line = $stdin.readline
    i, m, *vertices = line.split 
    
    vertex = g.vertex(i.to_i - 1)
    vertices.each do |b|
      vertex.addedge(b.to_i - 1)
    end
  end
  
  
  puts "graph #{graph_number+1}"
  while true
    v, type = $stdin.readline.split
    if v=='0' and type=='0'
      break
    end
    
    if type=='0'
      traversed = g.dfs(v.to_i - 1)
    else
      traversed = g.bfs(v.to_i - 1)
    end
    #traversed = traversed.collect {|i| i+1}
    traversed.collect! {|i| i+1}
    puts (traversed.join ' ')    
  end
  
end
