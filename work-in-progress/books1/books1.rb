require 'scanf'

file = STDIN
file = open(ARGV[1]) if ARGV[0]=='-f'

t = file.gets.to_i
t.times do
  a, b = file.gets.scanf("%d%d")
  pages = file.gets.split.map(&:to_i)
  scribe_pages = b.times.map {|i| [i, pages[-b + i]]}.to_h
  scribe_loc = b.times.map {|i| [i, pages.size - b + i]}.to_h
  (a - b - 1).downto(0) do |i|
    scribe_loc[0] -= 1
    scribe_pages[0] += pages[i]
    j = 1
    while j < b && scribe_pages[j-1] > scribe_pages[j]
      scribe_pages[j-1] -= pages[scribe_loc[j]]
      scribe_pages[j] += pages[scribe_loc[j]]
      scribe_loc[j]-=1
      j+=1
    end
  end 
  s = 0
  pages.each_with_index do |p, i|
    if scribe_loc[s] == i
      print '/ ' if i > 0
      s += 1
    end
    print pages[i]
    print ' ' if i < pages.size - 1
  end
  print "\n"
end

