require 'scanf'
d = gets.to_i

factorials=[1]
f = 1
i = 1
while f % 100 > 0
	f = f*i
	factorials[i]=f
	i+=1
end

MAX_N = i-1

d.times do 
	n, k = gets.scanf("%d%d")
	
	nn = n > MAX_N ? 0 : factorials[n]
	kk = k > MAX_N ? 0 : factorials[k]
	d = n - k
	dd = d > MAX_N ? 0 : factorials[d]
	
	#TODO
	#if k>n ...
	#n==0, k==0
	
	if dd == 0 
		puts 'P'
	else
		denom = kk * dd
		v = nn / denom		
		puts (v%2==0 ? 'P' : 'N')
	end
end


