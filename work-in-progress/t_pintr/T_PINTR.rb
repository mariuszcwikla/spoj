require 'scanf'

def cross(x1, y1, x2, y2, x, y)
	dx1 = x2 - x1
	dx2 = x2 - x
	dy1 = y2 - y1
	dy2 = y2 - y
	
	return dx1 * dy2 - dy1 * dx2
end

def sign(a)
	return 1 if a > 0
	return -1 if a < 0
	return 0
end

while true do
	line = gets
	x1,y1,x2,y2,x3,y3,x,y = line.scanf("%d%d%d%d%d%d%d%d")
	break if x1==0 and y1==0 and	
			 x2==0 and y2==0 and
			 x3==0 and y3==0 and
			 x ==0 and y ==0
	
	d1 = cross(x1, y1, x2, y2, x, y)
	d2 = cross(x2, y2, x3, y3, x, y)
	d3 = cross(x3, y3, x1, y1, x, y)
	
	d1 = sign(d1)
	d2 = sign(d2)
	d3 = sign(d3)
	
	n = [d1,d2,d3].select {|d| d!=0}
	
	#puts "#{d1} #{d2} #{d3}"
	if n.empty?
		puts "E"	#degenerate case
	elsif n.size==3
		s = n[0]+n[1]+n[2]
		if s==3 or s==-3
			puts "I"
		else
			puts "O"
		end
	elsif n.size==2
		if n[0]+n[1]==2 or n[0]+n[1]==-2
			puts "E"
		else
			puts "O"
		end
	else
		puts "E"	#na wierzcholku
	end
end